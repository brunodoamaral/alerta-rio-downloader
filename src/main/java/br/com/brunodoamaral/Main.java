package br.com.brunodoamaral;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * Created by uq4e on 12/15/15.
 */
public class Main {

	final static DateFormat serverDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	final static DateFormat outputDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");

	private static final Logger log = getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		if(args.length != 1) {
			System.err.println("Please specify an output folder");
			System.exit(1);
		}

		File outputFolder = new File(args[0]) ;

		final int CONNECTION_TIMEOUT = 30 * 1000; // 30s in ms
		RequestConfig requestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(CONNECTION_TIMEOUT)
				.setConnectTimeout(CONNECTION_TIMEOUT)
				.setSocketTimeout(CONNECTION_TIMEOUT)
				.build();
		
		CloseableHttpClient httpclient = HttpClients.custom()
				.setDefaultRequestConfig(requestConfig)
//				.setProxy(new HttpHost("10.26.26.37", 8002))
				.build();

		int lastImage = 0;
		Date dtLastImage = new Date(0);

		log.info("Downloading all images...");
		for (int imageIndex = 0; imageIndex < 20; imageIndex++) {
			Optional<Date> dtLastModified = downlaodImageIndexAndReturnLastModified(httpclient, imageIndex, outputFolder, true) ;
			if ( dtLastModified.isPresent() && dtLastModified.get().after(dtLastImage) ) {
				dtLastImage = dtLastModified.get() ;
				lastImage = imageIndex ;
			}
		}

		log.info("Last image is {} with modification at {}", lastImage+1, dtLastImage.toString());

		while (true) {
			log.info("Waiting...");
			Thread.sleep(30 * 1000);    // 30s
			
			int nextImage = (lastImage+1) % 20 ;
			// Get headers (3rd param = false)
			Optional<Date> optDtNextImage = downlaodImageIndexAndReturnLastModified(httpclient, nextImage, outputFolder, false) ;
			if(optDtNextImage.isPresent()) {
				Date dtNextImage = optDtNextImage.get() ;
				if (dtNextImage.after(dtLastImage)) {
					// Download and save next image...
					downlaodImageIndexAndReturnLastModified(httpclient, nextImage, outputFolder, true);

					dtLastImage = dtNextImage;
					lastImage = nextImage;
				}
			}
		}
	}
	
	private static Optional<Date> downlaodImageIndexAndReturnLastModified(CloseableHttpClient httpclient, int imageIndex, File outputFolder, boolean writeToFile) {
		imageIndex = imageIndex + 1 ;   // Convert input index (0-based) to server's index (1-based)
		HttpUriRequest request = getImage(imageIndex, !writeToFile);
		log.info("Executing '{}'...", request);

		try (CloseableHttpResponse response = httpclient.execute(request))
		{
			HttpEntity responseEntity = response.getEntity() ;
			String strLastModified = response.getFirstHeader("Last-Modified").getValue();
			Date dtLastModified = serverDateFormat.parse(strLastModified);
			
			if(writeToFile) {
				String fileOutName = String.format("%s-%03d.png", outputDateFormat.format(dtLastModified), imageIndex);
				File fileOut = new File(outputFolder, fileOutName) ;
				try (OutputStream out = new FileOutputStream(fileOut);
				     InputStream image = responseEntity.getContent())
				{
					byte[] buffer = new byte[1024];
					int bytesRead;
					while ((bytesRead = image.read(buffer)) >= 0) {
						out.write(buffer, 0, bytesRead);
					}
				}
			} else {
				EntityUtils.consume(responseEntity) ;
			}

			log.info("Done!");
			
			return Optional.of(dtLastModified) ;
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty() ;
		}
	}

	private static HttpUriRequest getImage(int index, boolean onlyHeaders) {
		if (index < 1 || index > 20) throw new IndexOutOfBoundsException("Index must be between 1 and 20");
		String strUrl = String.format("http://alertario.rio.rj.gov.br/upload/Mapa/semfundo/radar%03d.png", index);
		return onlyHeaders ? new HttpHead(strUrl) : new HttpGet(strUrl);
	}
}
